package com.example.project_62160313

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.project_62160313.databinding.FragmentAllBagsBinding

class AllBags : Fragment() {
    private var binding: FragmentAllBagsBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentBinding = FragmentAllBagsBinding.inflate(inflater, container, false)
        binding = fragmentBinding
        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            allBags = this@AllBags

        }
    }
    fun goToProductOne(){
        findNavController().navigate(R.id.action_allBags_to_productOne)
    }

    fun goToProductTwo(){
        findNavController().navigate(R.id.action_allBags_to_productTwo)
    }
    fun goToProductThree(){
        findNavController().navigate(R.id.action_allBags_to_productThree)
    }
    fun goToProductFour(){
        findNavController().navigate(R.id.action_allBags_to_productFour)
    }
    fun goToProductFive(){
        findNavController().navigate(R.id.action_allBags_to_productFive)
    }
    fun goToProductSix(){
        findNavController().navigate(R.id.action_allBags_to_productSix)
    }
}